#!/usr/bin/python

import paho.mqtt.client as mqtt

import logging
import time
import sys


logging.basicConfig(level=logging.INFO)

SERVER = sys.argv[1]
SIZE = int(sys.argv[2])
NUMBER = int(sys.argv[3])
WAIT = float(sys.argv[4])

def on_connect(mqttc, obj, flags, rc):
    logging.debug("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    logging.debug(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    logging.debug("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    logging.debug("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logging.debug(string)


# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
mqttc.connect(SERVER, 1883, 60)

mqttc.loop_start()
start = time.time()
for i in range(NUMBER):
    (rc, mid) = mqttc.publish("test", "a" * SIZE, qos=0)
    time.sleep(WAIT)

end = time.time()
total = end - start
rate = NUMBER / float(total)
print("total = {}".format(total))
print("rate = {}".format(rate))
