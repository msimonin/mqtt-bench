from enoslib.api import play_on, wait_ssh, ensure_python3
from enoslib.infra.enos_vmong5k.provider import VMonG5k
from enoslib.infra.enos_vmong5k.configuration import Configuration

import logging
import os

logging.basicConfig(level=logging.DEBUG)

CLUSTER = "paravance"
NB_VMS = 4

def bench(nb_vms: int) -> None:
    # claim the resources
    conf = Configuration.from_settings(job_name="enoslib_tutorial_", gateway=True)\
                        .add_machine(roles=["server"],
                                    cluster=CLUSTER,
                                    number=nb_vms,
                                    flavour="tiny")\
                        .add_machine(roles=["client"],
                                    cluster=CLUSTER,
                                    number=nb_vms,
                                    flavour="tiny")\
                        .finalize()

    provider = VMonG5k(conf)

    roles, networks = provider.init()
    wait_ssh(roles)


    servers = roles["server"]
    clients = roles["client"]

    for s, c in zip(servers, clients):
        c.extra.update(target=s.address)

    # Install python3 and make it the default
    ensure_python3(roles=roles)

    with play_on(roles=roles) as p:
        p.apt_repository(repo="deb http://deb.debian.org/debian stretch main contrib non-free",
                        state="present")
        p.apt(name=["flent", "netperf", "python3-setuptools", "python3-matplotlib", "tmux"],
            state="present")

    with play_on(pattern_hosts="server", roles=roles) as p:
        p.shell("tmux new-session -d 'exec netperf'")

    with play_on(pattern_hosts="client", roles=roles) as p:
        p.shell("flent tcp_upload -p totals "
                + "-l 60 "
                + "-H {{ target }} "
                + f"-t 'tcp_upload_{nb_vms}' "
                + f"-o tcp_upload_{nb_vms}.png", display_name=f"Benchmarkings with {nb_vms} vms")
        p.fetch(src=f"tcp_upload_{nb_vms}.png", dest=f"result_{nb_vms}")

for nb_vms in [1, 4, 8, 16]:
    bench(nb_vms)
