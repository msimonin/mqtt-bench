from enoslib.api import play_on, wait_ssh, ensure_python3, __docker__, run_command
from enoslib.infra.enos_vmong5k.provider import start_virtualmachines
import enoslib.infra.enos_vmong5k.configuration as vmconf
from enoslib.infra.enos_g5k.configuration import (Configuration,
                                                  NetworkConfiguration)
from enoslib.infra.enos_g5k.provider import G5k


import json
import logging
import os
import sys


# TODO parameterize this
NB_VMS = 16
CLUSTER = "parasilo"
size = 1000
number = 100000
wait = 0.001


logging.basicConfig(level=logging.DEBUG)


# claim the resources
conf = Configuration.from_settings(job_type="allow_classic_ssh",
                                   job_name="mqtt",
                                   walltime="03:00:00")
network = NetworkConfiguration(id="n1",
                               type="prod",
                               roles=["my_network"],
                               site="rennes")
(conf.add_network_conf(network)
 .add_network(id="not_linked_to_any_machine",
              type="slash_22",
              roles=["my_subnet"],
              site="rennes")
 .add_machine(roles=["server"],
              cluster=CLUSTER,
              nodes=1,
              primary_network=network)
 .add_machine(roles=["client"],
              cluster=CLUSTER,
              nodes=1,
              primary_network=network)
 .finalize())


provider = G5k(conf)

roles, networks = provider.init()
ensure_python3(roles=roles)

# start some vms
subnets = [n for n in networks if "my_subnet" in n["roles"]]
virt_conf = vmconf.Configuration.from_settings(gateway=True)
virt_conf.add_machine(roles=["client"],
                     number=NB_VMS,
                     undercloud=roles["client"])\
         .finalize()

vmroles, networks =start_virtualmachines(virt_conf, subnets, force_deploy=False)
wait_ssh(vmroles)

# Start the mqtt server
with play_on(pattern_hosts="server", roles=roles, priors=[__docker__]) as p:
    p.pip(name="docker", state="present")
    p.docker_container(name="mqtt-server",
                       image="eclipse-mosquitto",
                       network_mode="host",
                       state="started")

# prepare the publisher
server = roles["server"][0]
with play_on(pattern_hosts="client", roles=vmroles, gather_facts="all") as p:
    p.apt(name="python-pip", state="present")
    p.copy(src="pub.py", dest="pub.py")
    p.pip(name="paho-mqtt")


result=run_command(f"python pub.py {server.address} {size} {number} {wait}", pattern_hosts="client", roles=vmroles)

with open(f"result_{NB_VMS}", "w") as f:
    f.write(json.dumps(result["ok"], indent=2))
