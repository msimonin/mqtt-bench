from enoslib.api import discover_networks, play_on, ensure_python3
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import (Configuration,
                                                  NetworkConfiguration)

import logging


logging.basicConfig(level=logging.DEBUG)

network = NetworkConfiguration(id="n1",
                               type="prod",
                               roles=["mynetwork"],
                               site="rennes")

conf = (Configuration.from_settings(job_name="flent_on", job_type="allow_classic_ssh")
        .add_network_conf(network)
        .add_machine(roles=["server"],
                     cluster="paravance",
                     nodes=1,
                     primary_network=network)
        .add_machine(roles=["client"],
                     cluster="paravance",
                     nodes=1,
                     primary_network=network)
        .finalize())

provider = G5k(conf)
roles, networks = provider.init()

ensure_python3(roles=roles)

with play_on(roles=roles) as p:
    p.apt_repository(repo="deb http://deb.debian.org/debian stretch main contrib non-free",
                     state="present")
    p.apt(name=["flent", "netperf", "python3-setuptools", "python3-matplotlib"],
          state="present")

with play_on(pattern_hosts="server", roles=roles) as p:
    p.shell("nohup netperf &")

with play_on(pattern_hosts="client", roles=roles, gather_facts="all") as p:
    p.shell("flent tcp_upload -p totals "
            + "-l 60 "
            + "-H {{ hostvars[groups['server'][0]].ansible_default_ipv4.address }} "
            + "-t 'tcp_upload baseline' "
            + "-o result_baseline.png")
    p.fetch(src="result.png",
            dest="result")
